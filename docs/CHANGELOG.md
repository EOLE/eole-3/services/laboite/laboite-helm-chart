# Changelog

## [1.13.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.12.4...release/1.13.0) (2025-03-03)


### Features

* publish stable version 6.2.0 for laboite and dependencies ([1086895](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/1086895359db8caab707411334231f18b4f7272e))

### [1.12.4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.12.3...release/1.12.4) (2025-01-30)


### Bug Fixes

* update subcharts dependencies ([6a6cc1e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/6a6cc1ed8be9be9d7598c144a0383f876f71952e))

### [1.12.3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.12.2...release/1.12.3) (2024-10-25)


### Bug Fixes

* deploy laboite version 6.0.1 ([15a570f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/15a570fd37d62db99f48c4f4e924c4f3a00f202e))

### [1.12.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.12.1...release/1.12.2) (2024-10-15)


### Bug Fixes

* dev chart deploys dev appVersion ([e332b1f](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/e332b1fb47e7a7162e1b02002d88fa49417227a1))
* publish laboite for appVersion 6.0.0 ([b918415](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/b918415892c252812b6e50f851e05a4604d597d0))

### [1.12.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.12.0...release/1.12.1) (2024-09-30)


### Bug Fixes

* support mongodb complex password ([0ccc7d1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/0ccc7d1b2ac0a2507c99890bb0bba51462b594c6))

## [1.12.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.11.3...release/1.12.0) (2024-09-30)


### Features

* add headless service ([ccc2f16](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/ccc2f1667490f361b205aefdb6fa8b8cd4109205))


### Bug Fixes

* dev chart deploys dev subcharts ([714e547](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/714e547bfba6482b5386c4eb5ed94c172a9758d5))
* stable version deploy subcharts stable version ([16df007](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/16df00764ab9d969ea548034256dc1a92ae41d93))
* testing version deploy testing subcharts versions ([a9c5eed](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/a9c5eed143ec39cf480c2ed87ac87fb16c3bfc5a))
* use permanent redirect instead 302 ([a525beb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/a525beb38ad3dcc5033838e859ec8a544ba41553))

### [1.11.3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.11.2...release/1.11.3) (2024-07-19)


### Bug Fixes

* deploy stable images of lookupserver and radicale ([6a0dd59](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/6a0dd59a78c6b1dd1be544a6e11b42be4e4ac8fa))

### [1.11.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.11.1...release/1.11.2) (2024-07-09)


### Bug Fixes

* update laboite version to 5.9.1 ([2451a6a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/2451a6a4eebe050dd98b9a9b606657991f1d52c9))

### [1.11.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.11.0...release/1.11.1) (2024-06-17)


### Bug Fixes

* **84:** add possibility for mongo_url secret ([cbc6223](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/cbc6223d044a225ae2821067f61752a7e606a455))
* publish testing version ([2dd0338](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/2dd0338edd539ebbac425af145d584bc925d36fa))
* wait 5 minutes before killing pod ([4345e6d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/4345e6de03a4b85a54bdcbf89e14c10c856b61ff))

## [1.11.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.10.0...release/1.11.0) (2024-06-17)


### Features

* publish new helm stable version ([b7bf8aa](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/b7bf8aaaaa8976f203a97c2a2220e0da12b04525))

## [1.10.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.9.3...release/1.10.0) (2024-04-12)


### Features

* add possibility to add environment variable ([51bc1fc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/51bc1fcc7989db0bf9ee13df21e4b48eae4f7c1b))


### Bug Fixes

* remove unwanted subcharts ([5afc300](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/5afc300326c45168176dadeb89cc8fdd14e3d55e))

### [1.9.3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.9.2...release/1.9.3) (2024-03-11)


### Bug Fixes

* publish new stable version for laboite 5.8.2 ([be6167b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/be6167b1bc93cde2d106013586a1c924ecafa2cd))

### [1.9.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.9.1...release/1.9.2) (2024-02-21)


### Bug Fixes

* deploy laboite appversion 5.8.1 ([71f7055](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/71f7055f47358360e5634d6c7f53a5395edac1d6))

### [1.9.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.9.0...release/1.9.1) (2024-02-05)


### Bug Fixes

* update questionnaire dependency ([646dcbd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/646dcbd4dfa2084771d1d8c7310b2c71baa50fe0))

## [1.9.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.8.2...release/1.9.0) (2024-01-22)


### Features

* update appversion and dependencies ([df986fc](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/df986fc102a2c3e43eea250b96a00d1d2ae91d9c))

### [1.8.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.8.1...release/1.8.2) (2023-12-06)


### Bug Fixes

* bump app version to 5.7.2 ([95999e1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/95999e1319bd346abb3506db67ed84189ffa8313))

### [1.8.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.8.0...release/1.8.1) (2023-11-21)


### Bug Fixes

* deploy lookup-server subchart without api_key ([627fa92](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/627fa92a923342c636c33946da41e36002b57f91))

## [1.8.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.7.0...release/1.8.0) (2023-11-08)


### Features

* **radicale:** update radicale dependency to 1.1.0 ([b55f0cd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/b55f0cd16bb8619afadfed43b2ea19fdb2aa8b6c))

## [1.7.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.6.2...release/1.7.0) (2023-11-07)


### Features

* add default values for questionnaire cron ([d052728](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/d05272822ca8a2b6edd545c45bf4cbe32fa87d3d))
* add laboiteapi as subchart ([a1d905d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/a1d905da621739e805df8b860b38f2a84b66137b))
* add mongo dbname in secret for questionnaire cron ([4ac6d92](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/4ac6d92e18fd0f3e63c98a78e05d1e7e5a0594cd))
* new chart for laboite v11 ([7496364](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/7496364d8b23de9327dff4c700e0cce580fe5b6c))
* update radicale subchart dependency ([0ce56f4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/0ce56f458fc875e90eda3daea5fde12349eb71e0))


### Bug Fixes

* add laboiteapi values ([3ccd58a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/3ccd58afc11d21f56096fa0e1e02065fb172afb2))
* appversion reference testing image ([7d46324](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/7d4632415dd73aa1882e3263e0c1856a78c7f828))
* change chart-version for laboite-api subchart ([8e0ded3](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/8e0ded3ba40670f7a051d21451670116227f43f5))
* change laboiteapi to laboite-api in Chart.yaml ([b8f866e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/b8f866e206cdaa8f1ba1f069857c296da7b5480b))
* helm dev deploy dev image tag ([4731117](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/4731117436b9b4784d83c66b9ad0836f3627dd1f))
* helm dev deploy dev image tag ([12c7e3a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/12c7e3abdf3a0d0cb87c08c946a0977a40635411))
* set correct name for laboiteapi chart ([c13935d](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/c13935d66f286bc746f66ea7b070f0aaa8c2531d))
* update the dependencies version ([3fd5f87](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/3fd5f878eb143de063569bd9773d59a26615d38d))


### Styles

* default theme is now eole ([11c8075](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/11c80758424be73fbf51b697b170b48b587ee892))

### [1.6.2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.6.1...release/1.6.2) (2023-10-03)


### Bug Fixes

* deploy laboite 5.6.1 and blog 1.8.1 ([110c147](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/110c14723a6f75bd1d3cc230e91c5f7aa38e64c1))

### [1.6.1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.6.0...release/1.6.1) (2023-09-04)


### Bug Fixes

* deploy laboite 5.5.1 ([d4ca433](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/d4ca433b8f3b8d01152a6bed15a1c8993212bc10))

## [1.6.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.5.0...release/1.6.0) (2023-08-29)


### Features

* new helm version 1.4.0 for sondage ([2fd491a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/2fd491a6d0e0dd42295efc81cb95b0cabe8f8250))

## [1.5.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.4.0...release/1.5.0) (2023-08-25)


### Features

* new stable version for appversion 5.5.0 ([59f909b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/59f909bb8c696ed4050587c71b86f74eddd17312))

## [1.4.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.3.0...release/1.4.0) (2023-05-09)


### Features

* add ingress for redirect to portail ([ca6da2a](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/ca6da2a8430eaf73abeac9fd89fd4a93c5d88f55))
* bump app version to 5.4.0 ([378ec5b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/378ec5bc84e57082b375fdde7ecb782e27cd4197))
* include questionnaire as subchart ([31d94c4](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/31d94c4fbdf13253029920aa77a15358316426aa))
* publish new helm stable version ([f4bfa38](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/f4bfa38c2fe31fd732ccf81fb799ebc8358f5555))


### Bug Fixes

* add home var and bump to app version 5.4.0 ([2fac29e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/2fac29e98f648260cb73bf22bf82650853226750))
* correct helm version for helm subchart ([bcceae5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/bcceae5cce0a1910fdcfb44efcd4781189ec920b))
* publish new helm version ([efb6ec1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/efb6ec1d30df1c21ae98c1876a350ea9d34308a3))
* rebuild helm chart ([088e3ed](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/088e3ed03aa3ead1689cdadc72bb8064de71954f))
* set all subcharts version to the lastest dev ([69fc04c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/69fc04c48c2e1fa58b62866c56272c186f1cecfc))
* set all subcharts version to the lastest testing ([931344e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/931344e65253dc0a95e2c44fefecca991f187042))

## [1.3.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/compare/release/1.2.0...release/1.3.0) (2023-04-18)


### Features

* publish stable version for laboite 5.3.0 ([06cf408](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/laboite-helm-chart/commit/06cf408281d43fd0e2b095ebd20a356b0749162e))

## [1.2.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/compare/release/1.1.0...release/1.2.0) (2023-02-01)


### Features

* publish laboite 5.2.0 ([3162378](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/31623783de729f6d6523a4a608974d0f11719ed8))


### Bug Fixes

* chart version must be dev on dev branch ([b9bc328](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/b9bc3285e90286d29920c53470f1411eabb81544))
* dev chart version deploy dev app version ([0ec545c](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/0ec545ca87779b4e1f9d2a371effa4e2fb4e72e6))
* helm testing deploy testing app version ([d6c546b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/d6c546b8202ee474929abddb1e80980f164d07cc))
* remove mongodb backup ([06d8aa7](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/06d8aa7fea667031890d2910c5a4570989b03299))
* remove useless traefik files ([c972e39](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/c972e3992c28a3dc1950b7962bb0aace2d8149bd))
* update leblog and lookup-server chart version ([086752e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/086752ef3319a245270aa62271fde51aa3336b3e))

## [1.1.0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/compare/release/1.0.0...release/1.1.0) (2022-12-12)


### Features

* update subcharts dependencies ([78c5d2e](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/78c5d2e52997b95dc23ce7a73de9294ccde3a1e6))

## 1.0.0 (2022-12-12)


### Features

* **chart:** update dependencies to use `dev` helm ([cd4d7e9](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/cd4d7e956e542672d5b05a19b457b00df681b60e))
* **ci:** build helm package ([f48e598](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/f48e598cf4ffa467bc48e554fda126698a2bcca0))
* **ci:** push helm package to chart repository ([9d56470](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/9d56470d61a34fa8165e5df08db8e3336e8e9263))
* **ci:** update helm chart version with `semantic-release` ([a0e74d6](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/a0e74d67951da3cca222e7374fef5f5decd67035))
* **ci:** validate helm chart at `lint` stage ([46fff92](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/46fff924e0170ef81b00090c6fec2bc764e27682))
* content of meteor-settings secret is in values file ([0833546](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/083354654684da4e8dd301141a8056f2c78aabd0))
* **laboite:** add ui env parameter in meteor settings ([5af0eae](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/5af0eae4e2eb30af9c3757e0c08bf32cf31b50c9))
* **radicale:** add new env parameter ([dad82db](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/dad82dba6227cd804d0371d22198168a80401deb))
* update app version and subcharts dependencies ([62e4b58](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/62e4b586df618a3a101e7d9e2cc8876a3bcfe5a0))
* update blog subchart ([bf1eab1](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/bf1eab1d479f26a3765e1168d42483af9ef06967))
* use chart.yaml as default version ([4fc81b2](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/4fc81b2ea2a46dec26afc07eb23cb903b63937cd))
* use chart.yaml as default version ([7bffc23](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/7bffc2304543bdb5078fdffb10bad59af56edfb2))


### Bug Fixes

* **laboite:** set new subcharts dependencies ([2a565f8](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/2a565f8c5ef0e54386e7d5f5dcd27f7ae394c8a0))
* **laboite:** update addons new version ([bfe91dd](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/bfe91dd30d35994918f0fd8d0b0d37db9d9971c0))
* set correct appversion ([0f62878](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/0f628783da2ac439463120164bf0259d9994d2a4))
* update hpa api version removed on k8s 1.26 ([23ea31b](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/23ea31bfc26c4418f57c6084edac2655d0807d1c))
* **values:** add nextcloud groupurl parameter in meteor-settings ([74269c5](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/74269c5701fb4e881076ca67bed9bc06c1e9157d))
* **values:** add nextcloud values for user, password and quota ([b6ca9aa](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/b6ca9aa03f4c6fa7b1be38bbcf683638a4dd1029))


### Continuous Integration

* **commitlint:** enforce commit message format ([f7f4ebb](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/f7f4ebb0588bf2715cb14177ae334a1748d830de))
* **release:** create release automatically with `semantic-release` ([7f174b0](https://gitlab.mim-libre.fr/EOLE/eole-3/services/laboite/commit/7f174b0762d409d4580a5bc427150912d5e438f1))
